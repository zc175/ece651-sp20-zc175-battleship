package edu.duke.zc175.battleship;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.io.*;

public class TextPlayerTest {

    private TextPlayer createTextPlayer(int w, int h, String inputData, ByteArrayOutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
        V1ShipFactory shipFactory = new V1ShipFactory();
        return new TextPlayer("A", board, input, output, shipFactory);
    }

    @Test
    void test_read_placement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
      TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);

        String prompt = "Please enter a location for a ship:";
        Placement[] expected = new Placement[3];
        expected[0] = new Placement(new Coordinate(1, 2), 'V');
        expected[1] = new Placement(new Coordinate(2, 8), 'H');
        expected[2] = new Placement(new Coordinate(0, 4), 'V');

        for (int i = 0; i < expected.length; i++) {
            Placement p = player.readPlacement(prompt);
            assertEquals(p, expected[i]); //did we get the right Placement back

            // Todo: Change according to the system, "\r\n" for Windows and "\n" for linux
            assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline
            bytes.reset(); //clear out bytes for next time around
        }
    }


    // @Test
    // void test_do_one_placement() throws IOException {
    //     ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    //     TextPlayer player = createTextPlayer(10, 20, "B2V\nC7H\na4v\n", bytes);
    //     player.doOnePlacement();
    //     player.doOnePlacement();
    //     player.doOnePlacement();
    //     System.out.print(bytes.toString());
    // }
    @Test
    void test_possibleAction() throws IOException {
        Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        Board<Character> b2 = new BattleShipBoard<Character>(10, 20, 'X');
        BufferedReader input1 = new BufferedReader(new StringReader("M\nc5\nc6\ne4l\nS\nd5\nc4v\n"));
        BufferedReader input2 = new BufferedReader(new StringReader("F\nd5\ns\ng5"));

        // System.out.println(input.readLine());
        // System.out.println(input.readLine());
        // System.out.println(input.readLine());
        // System.out.println(input.readLine());
        // System.out.println(input.readLine());
        // System.out.println(input.readLine());


        V2ShipFactory factory = new V2ShipFactory();
        TextPlayer p1 = new TextPlayer("A", b1, input1, System.out, factory);
        TextPlayer p2 = new TextPlayer("B", b2, input2, System.out, factory);

        p1.setupShipCreationList();
        p1.setupShipCreationMap();

        p2.setupShipCreationList();
        p2.setupShipCreationMap();

        BoardTextView view = new BoardTextView(b1);
        BoardTextView view1 = new BoardTextView(b2);

        
        b1.tryAddShip(factory.makeBattleship(new Placement("c5u")));
        b1.tryAddShip(factory.makeCarrier(new Placement("e6u")));
        // p1.PossibleActions(p2);
        // p1.PossibleActions(p2);
        
        // System.out.print(view.displayMyOwnBoard());
        p2.PossibleActions(p1);
        b1.fireAt(new Coordinate("d5"));
        p1.PossibleActions(p2);
        System.out.print(view.displayMyOwnBoard());
        p2.PossibleActions(p1);
        b1.fireAt(new Coordinate("d5"));
        p2.PossibleActions(p1);

    }
}
