package edu.duke.zc175.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {
  @Test
  public void test_display_empty_2by2() {
    Board<Character> b1 = new BattleShipBoard<Character>(2, 2, 'X');
    BoardTextView view = new BoardTextView(b1);
    String expectedHeader= "  0|1\n";
    assertEquals(expectedHeader, view.makeHeader());
    String expected=
      expectedHeader+
      "A  |  A\n"+
      "B  |  B\n"+
      expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }

  @Test
  public void test_invalid_board_size() {
    Board<Character> wideBoard = new BattleShipBoard<Character>(11,20, 'X');
    Board<Character> tallBoard = new BattleShipBoard<Character>(10,27, 'X');
    //you should write two assertThrows here
  }

  private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
    Board<Character> b1 = new BattleShipBoard<Character>(w, h, 'X');
    BoardTextView view = new BoardTextView(b1);
    assertEquals(expectedHeader, view.makeHeader());
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }

  @Test
  public void test_display_empty_3by2() {
    String expectedHeader = "  0|1|2\n";
    String expectedBody = 
      "A  | |  A\n"+
      "B  | |  B\n";
    emptyBoardHelper(3, 2, expectedHeader, expectedBody);
  }

  @Test
  public void test_display_empty_3by5() {
    String expectedHeader = "  0|1|2\n";
    String expectedBody =
      "A  | |  A\n"+
      "B  | |  B\n"+
      "C  | |  C\n"+
      "D  | |  D\n"+
      "E  | |  E\n";
      emptyBoardHelper(3, 5, expectedHeader, expectedBody);
  }

  @Test
  void test_string_constructor_valid_cases() {
    Coordinate c1 = new Coordinate("B3");
    assertEquals(1, c1.getRow());
    assertEquals(3, c1.getColumn());
    Coordinate c2 = new Coordinate("D5");
    assertEquals(3, c2.getRow());
    assertEquals(5, c2.getColumn());
    Coordinate c3 = new Coordinate("A9");
    assertEquals(0, c3.getRow());
    assertEquals(9, c3.getColumn());
    Coordinate c4 = new Coordinate("Z0");
    assertEquals(25, c4.getRow());
    assertEquals(0, c4.getColumn());

  }
  @Test
  public void test_string_constructor_error_cases() {
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("00"));
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("AA"));
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("@0"));
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("[0"));
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("A/"));
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("A:"));
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("A"));
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("A12"));
  }

  @Test
  public void test_new_display() {
    Board<Character> b1 = new BattleShipBoard<Character>(3, 4, 'X');
    BoardTextView view = new BoardTextView(b1);
    Coordinate pos = new Coordinate(2, 1);
    Ship<Character> s = new RectangleShip<Character>("submarine", pos, 2, 2, 's', '*');
    b1.tryAddShip(s);
    String expected = 
      "  0|1|2\n"+
      "A  | |  A\n"+
      "B  | |  B\n"+
      "C  |s|s C\n"+
      "D  |*|s D\n"+
      "  0|1|2\n";

      String expected1 = 
      "  0|1|2\n"+
      "A  |X|  A\n"+
      "B  | |  B\n"+
      "C  | |  C\n"+
      "D  |s|  D\n"+
      "  0|1|2\n";
    
    b1.fireAt(new Coordinate("D1"));
    b1.fireAt(new Coordinate("a1"));
    assertEquals(expected, view.displayMyOwnBoard());
    assertEquals(expected1, view.displayEnemyBoard());
    assertEquals("submarine", s.getName());

  }

  @Test
  public void test_new_display_all() {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    Board<Character> b2 = new BattleShipBoard<Character>(10, 20, 'X');
    BoardTextView view1 = new BoardTextView(b1);
    BoardTextView view2 = new BoardTextView(b2);
    System.out.print(view1.displayMyBoardWithEnemyNextToIt(view2, "Your ocean", "Player B's ocean"));
    // assertEquals("     Your ocean                           Player B's ocean\n", view1.displayMyBoardWithEnemyNextToIt(view2, "Your ocean", "Player B's ocean"));
  }

}


