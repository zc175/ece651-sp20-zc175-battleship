package edu.duke.zc175.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class InBoundRuleCheckerTest {
    @Test
    public void checkMyRuleInBoundsTest() {
        Board<Character> b1 = new BattleShipBoard<Character>(5, 10, 'X');
        Ship<Character> s = new V1ShipFactory().makeDestroyer(new Placement(new Coordinate(9, 4), 'V'));
        Ship<Character> s1 = new V1ShipFactory().makeDestroyer(new Placement(new Coordinate(1, 2), 'V'));
        // Ship<Character> s2 = new V1ShipFactory().makeDestroyer(new Placement(new Coordinate(1, 4), 'H'));
        InBoundsRuleChecker<Character> checker = new InBoundsRuleChecker<Character>(null);
        // assertEquals(false, checker.checkPlacement(s, b1));
        // assertEquals(true, checker.checkPlacement(s1, b1));
        // assertDoesNotThrow(checker.checkPlacement(s, b1));
        // assertEquals(false, checker.checkPlacement(s2, b1));

    }

    @Test
    public void checkMyRuleTest() {
        Board<Character> b1 = new BattleShipBoard<Character>(5, 10, 'X');
        Ship<Character> s1 = new V1ShipFactory().makeDestroyer(new Placement(new Coordinate(1, 2), 'V'));
        NoCollisionRuleChecker<Character> checker = new NoCollisionRuleChecker<Character>(null);
        InBoundsRuleChecker<Character> checker2 = new InBoundsRuleChecker<Character>(checker);
        // assertEquals(false, checker2.checkPlacement(s, b1));
        // assertEquals(true, checker2.checkPlacement(s1, b1));
        // assertEquals(true, checker.checkPlacement(s1, b1));
    }

    @Test
    public void checkMyRuleNoCollisionTest() {
        Board<Character> b1 = new BattleShipBoard<Character>(5, 10, 'X');
        Ship<Character> s = new V1ShipFactory().makeDestroyer(new Placement(new Coordinate(9, 4), 'V'));
        Ship<Character> s1 = new V1ShipFactory().makeDestroyer(new Placement(new Coordinate(1, 2), 'V'));
        Ship<Character> s2 = new V1ShipFactory().makeDestroyer(new Placement(new Coordinate(1, 4), 'H'));
        Ship<Character> s3 = new V1ShipFactory().makeDestroyer(new Placement(new Coordinate(1, 1), 'H'));
        b1.tryAddShip(s3);
        InBoundsRuleChecker<Character> checker = new InBoundsRuleChecker<Character>(null);
        NoCollisionRuleChecker<Character> checker2 = new NoCollisionRuleChecker<Character>(checker);
        // assertEquals(false, checker2.checkPlacement(s, b1));
        // assertEquals(false, checker2.checkPlacement(s1, b1));
        // assertEquals(false, checker2.checkPlacement(s2, b1));
    }

    @Test
    public void checkModifiedTryAddShipTest() {
                InBoundsRuleChecker<Character> checker = new InBoundsRuleChecker<Character>(null);
        NoCollisionRuleChecker<Character> checker2 = new NoCollisionRuleChecker<Character>(checker);
        Board<Character> b1 = new BattleShipBoard<Character>(5, 10, checker2, 'X');
        Ship<Character> s = new V1ShipFactory().makeDestroyer(new Placement(new Coordinate(9, 4), 'V'));
        Ship<Character> s1 = new V1ShipFactory().makeDestroyer(new Placement(new Coordinate(1, 2), 'V'));
        Ship<Character> s2 = new V1ShipFactory().makeDestroyer(new Placement(new Coordinate(1, 4), 'H'));
        Ship<Character> s3 = new V1ShipFactory().makeDestroyer(new Placement(new Coordinate(1, 1), 'H'));
        // assertEquals(true, b1.tryAddShip(s3));
        // assertEquals(false, b1.tryAddShip(s));
        // assertEquals(false, b1.tryAddShip(s1));
        // assertEquals(false, b1.tryAddShip(s2));
        

    }
}
