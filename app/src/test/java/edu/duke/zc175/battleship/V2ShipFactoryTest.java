package edu.duke.zc175.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V2ShipFactoryTest {
    @Test
    public void createShipTest() {
        Board<Character> b1 = new BattleShipBoard<Character>(10, 10, 'X');
        BoardTextView view = new BoardTextView(b1);
        V2ShipFactory factory = new V2ShipFactory();
        Placement pos = new Placement(new Coordinate(2, 3), 'H');
        Ship<Character> s = factory.makeSubmarine(pos);
        b1.tryAddShip(s);


        Placement pos1 = new Placement(new Coordinate(5, 2), 'R');
        Ship<Character> s1 = factory.makeBattleship(pos1);
        b1.tryAddShip(s1);

        
        System.out.print(view.displayMyOwnBoard());
    }
}
