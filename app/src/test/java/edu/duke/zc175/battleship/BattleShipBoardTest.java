package edu.duke.zc175.battleship;

import static org.junit.jupiter.api.Assertions.*;

import javax.swing.plaf.TextUI;

import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {
  @Test
  public void test_width_and_height() {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    assertEquals(10, b1.getWidth());
    assertEquals(20, b1.getHeight());
  }

  @Test
  public void test_invalid_dimensions() {
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, 0, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(0, 20, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, -5, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20, 'X'));
  }

  private void checkWhatIsAtBoard(BattleShipBoard<Character> b) {
    int row_b = b.getHeight();
    int col_b = b.getWidth();
    for(int i=0; i<row_b; i++) {
      for(int j=0; j<col_b; j++) {
        assertNull(b.whatIsAtForSelf(new Coordinate(row_b, col_b)));
      }
    }
    Coordinate pos = new Coordinate(3, 4);
    Ship<Character> s = new RectangleShip<Character>("Destoryer", pos, 2, 3, 's', '*');
    b.tryAddShip(s);
    assertEquals('s', b.whatIsAtForSelf(pos));
    assertEquals("Destoryer", s.getName());

  }

  @Test
  public void test_whatIsAt() {
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(9, 20, 'X');
    checkWhatIsAtBoard(b1);
  }

  @Test
  public void fireAt() {
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    AbstractShipFactory<Character> factory = new V1ShipFactory();
    Ship<Character> s1 = factory.makeDestroyer(new Placement("a4h"));
    Ship<Character> s2 = factory.makeCarrier(new Placement("c5v"));
    b1.tryAddShip(s1);
    b1.tryAddShip(s2);

    assertNull(b1.fireAt(new Coordinate(0, 0)));
    assertSame(s1, b1.fireAt(new Coordinate("a5")));
    assertSame(s1, b1.fireAt(new Coordinate("a4")));
    assertSame(s1, b1.fireAt(new Coordinate("a6")));

    assertEquals(true, s1.isSunk());

    assertSame(s2, b1.fireAt(new Coordinate("d5")));
  }

  @Test
  public void test_new_display() {
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');

    AbstractShipFactory<Character> factory = new V1ShipFactory();
    Ship<Character> s1 = factory.makeDestroyer(new Placement("a4h"));
    Ship<Character> s2 = factory.makeSubmarine(new Placement("c5v"));
    b1.tryAddShip(s1);
    b1.tryAddShip(s2);

    assertNull(b1.fireAt(new Coordinate(0, 7)));
    assertSame(s1, b1.fireAt(new Coordinate("a5")));
    assertSame(s1, b1.fireAt(new Coordinate("a4")));


    assertSame(s2, b1.fireAt(new Coordinate("d5")));
    
    assertEquals('*', b1.whatIsAtForSelf(new Coordinate("a5")));
    assertEquals('d', b1.whatIsAtForSelf(new Coordinate("a6")));
    assertEquals('d', b1.whatIsAtForEnemy(new Coordinate("a5")));
    assertNull(b1.whatIsAtForEnemy(new Coordinate("a6")));
    assertEquals('X', b1.whatIsAtForEnemy(new Coordinate(0, 7)));

    b1.fireAt(new Coordinate("a6"));
    b1.fireAt(new Coordinate("c5"));
    
    assertEquals(true, b1.judgeLose());
  }

}