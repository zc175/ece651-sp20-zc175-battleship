package edu.duke.zc175.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V1ShipFactoryTest {
    @Test
    public void createShipTest() {
        Board<Character> b1 = new BattleShipBoard<Character>(10, 10, 'X');

        V1ShipFactory factory = new V1ShipFactory();
        Placement pos = new Placement(new Coordinate(2, 3), 'H');
        Ship<Character> s = factory.makeSubmarine(pos);
        // s.getDisplayInfoAt(null);
    }
}
