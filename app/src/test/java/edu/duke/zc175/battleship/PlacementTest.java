package edu.duke.zc175.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {
    @Test
    public void test_equals() {
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 2);


        Placement p1 = new Placement(c1, 'v');
        Placement p2 = new Placement(c2, 'V');
        Placement p4 = new Placement("b2v");
        Placement p5 = new Placement("b2u");
        assertEquals(p1, p2);
        String expected = "(1, 2)V";
        assertEquals(expected, p1.toString());
        assertEquals(p1, p4);
    }
}
