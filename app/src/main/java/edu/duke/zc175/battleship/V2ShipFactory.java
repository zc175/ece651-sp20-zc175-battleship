package edu.duke.zc175.battleship;

import java.util.ArrayList;
import java.util.HashMap;

public class V2ShipFactory implements AbstractShipFactory<Character> {

    /**
     * Map orientations to order of ship coordinate for battleship
     */
    final HashMap<Character, ArrayList<Coordinate>> BattleShipBlock = new HashMap<Character, ArrayList<Coordinate>>() {{
        put('U', new ArrayList<Coordinate>() {{ add(new Coordinate(0, 0)); add(new Coordinate(0, 2)); }});
        put('D', new ArrayList<Coordinate>() {{ add(new Coordinate(1, 0)); add(new Coordinate(1, 2)); }});
        put('R', new ArrayList<Coordinate>() {{ add(new Coordinate(0, 1)); add(new Coordinate(2, 1)); }});
        put('L', new ArrayList<Coordinate>() {{ add(new Coordinate(0, 0)); add(new Coordinate(2, 0)); }});
    }};

    /**
     * Map orientations to order of ship coordinate for carrier
     */
    final HashMap<Character, ArrayList<Coordinate>> CarrierBlock = new HashMap<Character, ArrayList<Coordinate>>() {{
        put('U', new ArrayList<Coordinate>() {{ add(new Coordinate(0, 1)); add(new Coordinate(1, 1)); add(new Coordinate(4, 0));}});
        put('D', new ArrayList<Coordinate>() {{ add(new Coordinate(0, 1)); add(new Coordinate(3, 0)); add(new Coordinate(4, 0));}});
        put('R', new ArrayList<Coordinate>() {{ add(new Coordinate(0, 0)); add(new Coordinate(1, 3)); add(new Coordinate(1, 4));}});
        put('L', new ArrayList<Coordinate>() {{ add(new Coordinate(0, 0)); add(new Coordinate(0, 1)); add(new Coordinate(1, 4));}});
    }};

    /**
     * Constructor to create rectangle ship, using rectangle ship constructor.
     * Default direction is Vertical, if orientation is Horizontal, need to switch widht and height
     * @param where is placement to place ship
     * @param w is width of ship
     * @param h is height of ship
     * @param letter is information showed on player's view 
     * @param name is type of the ship
     * @return
     */
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        char orient = where.getOrientation();
        if (orient != 'V' && orient != 'H') {
            throw new IllegalArgumentException("Direction for " + name + " is wrong");
        }
        if(orient == 'H') {
            int temp = w;
            w = h;
            h = temp;
        }
        return new RectangleShip<Character>(name, where.getWhere(), w, h, letter, '*');
    }


    /**
     * Make a submarine.
     * 
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the submarine.
     */
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    /**
     * Make a battleship.
     * 
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the battleship.
     */
    public Ship<Character> makeBattleship(Placement where) {

        char orient = where.getOrientation();
        int r = where.getWhere().getRow(), c = where.getWhere().getColumn();

        ArrayList<Coordinate> coords = new ArrayList<Coordinate>();

        if(orient == 'U') {
            coords.add(new Coordinate(r, c+1));
            for(int i=0; i<3; i++) {
                coords.add(new Coordinate(r+1, c+i));
            }
        }
        else if(orient == 'R') {
            coords.add(new Coordinate(r+1, c+1));
            for(int i=0; i<3; i++) {
                coords.add(new Coordinate(r+i, c));
            }
        }
        else if(orient == 'D') {
            coords.add(new Coordinate(r+1, c+1));
            for(int i=2; i>=0; i--) {
                coords.add(new Coordinate(r, c+i));
            }
        }
        else if(orient == 'L') {
            coords.add(new Coordinate(r+1, c));
            for(int i=2; i>=0; i--) {
                coords.add(new Coordinate(r+i, c+1));
            }
        }
        else {
            throw new IllegalArgumentException("Direction for Battleship is wrong");
        }

        return new IrregularShips<Character>("Battleship", coords, 'b', '*');
    }

    /**
     * Make a carrier.
     * 
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the carrier.
     */
    public Ship<Character> makeCarrier(Placement where) {

        char orient = where.getOrientation();
        int r = where.getWhere().getRow(), c = where.getWhere().getColumn();

        ArrayList<Coordinate> coords = new ArrayList<Coordinate>();

        if(orient == 'U') {
            for(int i=0; i<4; i++) {
                coords.add(new Coordinate(r+i, c));
            }
            for(int i=0; i<3; i++) {
                coords.add(new Coordinate(r+2+i, c+1));
            }
        }
        else if(orient == 'R') {
            for(int i=0; i<4; i++) {
                coords.add(new Coordinate(r, c+4-i));
            }
            for(int i=0; i<3; i++) {
                coords.add(new Coordinate(r+1, c+2-i));
            }
        }
        else if(orient == 'D') {
            for(int i=0; i<4; i++) {
                coords.add(new Coordinate(r+4-i, c+1));
            }
            for(int i=0; i<3; i++) {
                coords.add(new Coordinate(r+2-i, c));
            }
        }
        else if(orient == 'L') {
            for(int i=0; i<4; i++) {
                coords.add(new Coordinate(r+1, c+i));
            }
            for(int i=0; i<3; i++) {
                coords.add(new Coordinate(r, c+2+i));
            }
        }
        else {
            throw new IllegalArgumentException("Direction for Carrier is wrong");
        }

        return new IrregularShips<Character>("Carrier", coords, 'c', '*');
    }

    /**
     * Make a destroyer.
     * 
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the destroyer.
     */
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }
}
