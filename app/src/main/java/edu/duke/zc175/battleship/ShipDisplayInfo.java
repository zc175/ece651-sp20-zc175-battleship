package edu.duke.zc175.battleship;

public interface ShipDisplayInfo<T> {
    /**
     * get information of the coordinate on board, with hit or not showed.
     */
    public T getInfo(Coordinate where, boolean hit);
}
