package edu.duke.zc175.battleship;

public class V1ShipFactory implements AbstractShipFactory<Character> {

    /**
     * Constructor to create rectangle ship, using rectangle ship constructor.
     * Default direction is Vertical, if orientation is Horizontal, need to switch widht and height
     * @param where is placement to place ship
     * @param w is width of ship
     * @param h is height of ship
     * @param letter is information showed on player's view 
     * @param name is type of the ship
     * @return
     */
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        if(where.getOrientation() == 'H') {
            int temp = w;
            w = h;
            h = temp;
        }
        return new RectangleShip<Character>(name, where.getWhere(), w, h, letter, '*');
    }


    /**
     * Make a submarine.
     * 
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the submarine.
     */
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    /**
     * Make a battleship.
     * 
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the battleship.
     */
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 1, 4, 'b', "Battleship");
    }

    /**
     * Make a carrier.
     * 
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the carrier.
     */
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 1, 6, 'c', "Carrier");
    }

    /**
     * Make a destroyer.
     * 
     * @param where specifies the location and orientation of the ship to make
     * @return the Ship created for the destroyer.
     */
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }
}
