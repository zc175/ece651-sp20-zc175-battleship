package edu.duke.zc175.battleship;

import java.util.ArrayList;

public class IrregularShips<T> extends BasicShip<T> {
    

    // Ship name
    private final String name;


    /**
     * Constructor using BasicShip class constructor, define the rectangle ship
     * @param name is the name of ship
     * @param upperLeft is the upperleft coordinate of rectangle ship
     * @param width is the width of rectangle shiop
     * @param height is the height of rectangle ship
     * @param myDisplayInfo is information used for player's view
     * @param enemyDisplayInfo is information used for enemy's view
     */
    public IrregularShips(String name, ArrayList<Coordinate> coords, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(coords, myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    /**
     * Constructor with myDisplayInfo and enemyDisplayInfo predefined
     */
    public IrregularShips(String name, ArrayList<Coordinate> coords, T data, T onHit) {
        this(name, coords, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
    }

    /**
     * Return name of the ship
     */
    public String getName() {
        return this.name;
    }
}
