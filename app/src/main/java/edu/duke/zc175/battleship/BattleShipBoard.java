package edu.duke.zc175.battleship;

import java.util.*;

public class BattleShipBoard<T> implements Board<T> {
    
    /**
     * width of board
     */
    private final int width;

    /**
     * height of board
     */
    private final int height;

    /**
     * ships of the player on this board
     */
    private final ArrayList<Ship<T>> myShips;

    /**
     * rules to check validity of placement of the ship
     */
    private final PlacementRuleChecker<T> placementChecker;

    /**
     * coordinates that enemy miss the mark
     */
    private HashSet<Coordinate> enemyMisses = new HashSet<Coordinate>();

    /**
     * Coordinates that enemy hit (so that still shows in same place even the ship moves away)
     */
    private HashMap<Coordinate, T> enemyHit = new HashMap<Coordinate, T>();

    /**
     * information to show on enemy's view for missing the attack
     */
    private final T missInfo;


    /**
     * Construct a BattleShipBoard with the specified width and height.
     * @param init_height is the height of the newly constructed board.
     * @param init_width is the width of the newly constructed board.
     * @param rules is the rules used for checking whether the placement is valid or not
     * @param missInfo is the information showed on enemy board when they missed for attack
     * @throw IllegalArguementException if the width or height are less than or equal to zeor.
     */
    public BattleShipBoard(int init_width, int init_height, PlacementRuleChecker<T> rules, T missInfo) {
        if (init_width <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + init_width);
        }
        if (init_height <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + init_height);
        }
        height = init_height;
        width = init_width;
        myShips = new ArrayList<Ship<T>>();
        placementChecker = rules;
        this.missInfo = missInfo;
    }

    /**
     * Construct a BattleShipBoard with the specified width and height.
     * 
     * @param w is the width of board
     * @param h is the height of board
     * @param missInfo is information showed when enemy miss the mark
     */
    public BattleShipBoard(int w, int h, T missInfo) {
        this(w, h, new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<T>(null)), missInfo);
    }


    /**
     * Get the ship that contains input cooridnate
     * 
     * @param c is the coordinate to check
     * @return ship that contains the coordinate or null
     */
    public Ship<T> getShipAt(Coordinate c) {
        for(Ship<T> s : myShips) {
            if(s.occupiesCoordinates(c)) {
                return s;
            }
        }
        return null;
    }

    /**
     * Remove ship from board
     * @param s is the ship to be removed
     */
    public void removeShip(Ship<T> s) {
        myShips.remove(s);
    }


    /**
     * Scan at the input coordinate
     * 
     * @param c is the center of the scan area
     * @return the result of scan, how many blocks each kind of ship occupies
     */
    public HashMap<String, Integer> ScanAt(Coordinate c) {
        HashMap<String, Integer> res = new HashMap<String, Integer>() {{
            put("Submarine", 0);
            put("Destroyer", 0);
            put("Battleship", 0);
            put("Carrier", 0);
        }};
        int row = c.getRow(), col = c.getColumn();
        for(int i=0; i<4; i++) {
            for(int j = 0; j < 4-i; j++) {
                ArrayList<Integer> possibleRow = new ArrayList<Integer>();
                ArrayList<Integer> possibleCol = new ArrayList<Integer>();
                if(i != 0) {
                    possibleRow.add(row-i);
                }
                possibleRow.add(row+i);
                if(j != 0) {
                    possibleCol.add(col-j);
                }
                possibleCol.add(col+j);

                for(int rowSelect : possibleRow) {
                    for(int colSelect : possibleCol) {
                        Coordinate coordinateSelected = new Coordinate(rowSelect, colSelect);
                        // System.out.println(coordinateSelected.toString());
                        Ship<T> s = getShipAt(coordinateSelected);
                        if(s != null) {
                            
                            int val = res.get(s.getName());
                            res.replace(s.getName(), val+1); 
                        }
                    }
                }
            }
        }

        return res;
    }

    
    /**
     * Fire at the coordinate
     * 
     * @param c is the coordinate to fire at
     * @return the hit ship or null for missing the mark
     */
    public Ship<T> fireAt(Coordinate c) {
        //TODO
        Ship<T> s = getShipAt(c);
        if (s != null) {
            s.recordHitAt(c);
            enemyHit.put(c, s.getDisplayInfoAt(c, false));
            enemyMisses.remove(c);
            return s;
        }
        else {
            enemyHit.remove(c);
            enemyMisses.add(c);
            return null;
        }
    }

    /**
     * try to add one ship into board, check whethter the coordinates of ship is valid
     * 
     * @param toAdd is the ship to be add
     */
    public void tryAddShip(Ship<T> toAdd) throws IllegalArgumentException {
        placementChecker.checkPlacement(toAdd, this);
        this.myShips.add(toAdd);
        return;
    }

    /**
     * Get information showed on player's own board at coordinate
     * 
     * @param where is the coordinate to get information
     * @return the information at coordinate
     */
    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    /**
     * Get information showed on enemy's view of player's board at coordinate
     * 
     * @param where is the coordinate to get information
     * @return the information at coordinate
     */
    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }

    /**
     * Get information of board at input coordinate
     * need to check both ships and enemy fires, in order to show the misses made by enemy on enemy's view
     * 
     * @param where is the coordinate to get information
     * @param isSelf is the identifier that whether we want to get information of own view or enemy view
     * @return the information of the coordinate on board
     */
    protected T whatIsAt(Coordinate where, boolean isSelf) {
        if(!isSelf) {
            if (enemyMisses.contains(where)) {
               return missInfo; 
            }
            else if (enemyHit.containsKey(where)) {
                return enemyHit.get(where);
            }
            else {
                return null;
            }
        }


        if (!myShips.isEmpty()) {
            for (Ship<T> s: myShips) {
                if (s.occupiesCoordinates(where)){
                    return s.getDisplayInfoAt(where, isSelf);
                }
            }
        }
              
        return null;
    }


    /**
     * Return the width of the board.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Return the height of the board.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Chech whether the player lose
     */
    public boolean judgeLose() {
        for(Ship<T> s : myShips) {
            if(!s.isSunk()) {
                return false;
            }
        }
        return true;
    }

}
