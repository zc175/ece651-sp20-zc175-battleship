package edu.duke.zc175.battleship;

public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {

    /**
     * Constructor extend from PlacementRuleChecker
     */
    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }


    /**
     * check whether the ship placement is overlaped with other ships
     * @param theShip is the ship placement to be checked
     * @param theBoard is the board used
     */
    @Override
    protected void checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        // System.out.println("Checking collision");
        for(Coordinate c : theShip.getCoordinates()) {
            if(theBoard.whatIsAtForSelf(c) != null) {
                // System.out.println("Occupied by others from NoCollisionChecker\n");
                // return false;
                throw new IllegalArgumentException("the ship overlaps another ship.");
            }
        }
        return;
    }

    
    
}
