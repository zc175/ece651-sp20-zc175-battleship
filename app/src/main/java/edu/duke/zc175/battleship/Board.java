package edu.duke.zc175.battleship;

import java.util.HashMap;

public interface Board<T> {
    /**
     * Get width of board
     * @return width of board
     */
    public int getWidth();

    /**
     * Get height of board
     * @return height of board
     */
    public int getHeight();

    /**
     * Get information of coordinate showed on player's own view
     * @param where is coordinate to get information
     * @return information showed at coordinate on player's view
     */
    public T whatIsAtForSelf(Coordinate where);

    /**
     * Get information of coordinate showed on enemy's view
     * @param where is coordinate to get information
     * @return information showed at coordinate on enemy's view
     */
    public T whatIsAtForEnemy(Coordinate where);
    // public boolean tryAddShip(Ship<T> toAdd);

    /**
     * Try to add one ship to board
     * @param toAdd is the ship to be added
     */
    public void tryAddShip(Ship<T> toAdd);

    /**
     * Get the ship that contains input cooridnate
     * 
     * @param c is the coordinate to check
     * @return ship that contains the coordinate or null
     */
    public Ship<T> getShipAt(Coordinate c);


    /**
     * Remove ship from board
     * @param s is the ship to be removed
     */
    public void removeShip(Ship<T> s);


    /**
     * Scan at the input coordinate
     * 
     * @param c is the center of the scan area
     * @return the result of scan, how many blocks each kind of ship occupies
     */
    public HashMap<String, Integer> ScanAt(Coordinate c);

    /**
     * Fire at input coordinate
     * @param c is the marked coordinate
     * @return ship hit by fire or null for missing the mark
     */
    public Ship<T> fireAt(Coordinate c);

    /*
     * Judge whether the player is lose
     */
    public boolean judgeLose();
}
