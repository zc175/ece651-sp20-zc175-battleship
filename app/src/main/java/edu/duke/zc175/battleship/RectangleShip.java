package edu.duke.zc175.battleship;

import java.util.*;


public class RectangleShip<T> extends BasicShip<T> {

    /**
     * Generate coordinates of rectangle ship
     * @param upperLeft is the upperleft coordinate of rectangle ship
     * @param width is the width of rectangle shiop
     * @param height is the height of rectangle ship
     * @return hashset of all coordinates of the ship
     */
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
        HashSet<Coordinate> myCoords = new HashSet<Coordinate>();
        int row_base = upperLeft.getRow();
        int col_base = upperLeft.getColumn();
        for(int i=0; i<width; i++) {
            for(int j=0; j<height; j++) {
                Coordinate newCoord = new Coordinate(row_base + j, col_base+i);
                myCoords.add(newCoord);
            }
        }
        return myCoords;
    }


    // Ship name
    private final String name;


    /**
     * Constructor using BasicShip class constructor, define the rectangle ship
     * @param name is the name of ship
     * @param upperLeft is the upperleft coordinate of rectangle ship
     * @param width is the width of rectangle shiop
     * @param height is the height of rectangle ship
     * @param myDisplayInfo is information used for player's view
     * @param enemyDisplayInfo is information used for enemy's view
     */
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeCoords(upperLeft, width, height), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    /**
     * Constructor with myDisplayInfo and enemyDisplayInfo predefined
     */
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
    }

    /**
     * Constructor with name, myDisplayInfo and enemyDisplayInfo predefined
     */
    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship", upperLeft, 1, 1, data, onHit);
    }

    /**
     * Return name of the ship
     */
    public String getName() {
        return this.name;
    }
    
}
