package edu.duke.zc175.battleship;


public class Coordinate {

    /**
     * row of coordinate
     */
    private final int row;

    /**
     * column of coordinate
     */
    private final int col;

    /**
     * get row of coordinate
     */
    public int getRow() {
        return this.row;
    }

    /**
     * get column of coordinate
     */
    public int getColumn() {
        return this.col;
    }

    /**
     * Constructor of coordinate
     * @param r is row of coordinate
     * @param c is column of coordinate
     */
    public Coordinate(int r, int c) {
        this.row = r;
        this.col = c;
    }

    /**
     * Constructor of coordiante
     * @param descr is string form of coordinate, needs validity checking
     */
    public Coordinate(String descr) {
        if (descr.length() != 2) {
            throw new IllegalArgumentException("it does not have the correct format.");
            // throw new IllegalArgumentException("Wrong lenth of descr, need length of 2 while got: " + descr);
        }
        descr = descr.toUpperCase();
        char rowLetter = descr.charAt(0);
        char colLetter = descr.charAt(1);
        
        if (rowLetter < 'A' || rowLetter > 'Z') {
            throw new IllegalArgumentException("it does not have the correct format.");
            // throw new IllegalArgumentException("Wrong letter for row: " + rowLetter);
        }

        if (colLetter < '0' || colLetter > '9') {
            throw new IllegalArgumentException("it does not have the correct format.");
            // throw new IllegalArgumentException("Wrong letter for column: " + colLetter);
        }
        
        this.row = rowLetter - 'A';
        this.col = colLetter - '0';
    }
    
    /**
     * Check whether input coordinate is same as this coordinate
     * @param o is the input coordinate
     * @return boolean value that whether the two coordiantes are the same
     */
    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Coordinate c = (Coordinate) o;
            return row == c.row && col == c.col;
        }
        return false;
    }

    /**
     * Return string view of coordinate
     */
    @Override
    public String toString() {
        return "("+row+", " + col+")";
    }

    /**
     * Return hash code of coordinate
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
