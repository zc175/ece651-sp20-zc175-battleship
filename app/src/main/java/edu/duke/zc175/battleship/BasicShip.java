package edu.duke.zc175.battleship;

import java.util.*;

public abstract class BasicShip<T> implements Ship<T> {

    /**
     * Coordinates of ship and whether it is hit
     */
    protected HashMap<Coordinate, Boolean> myPieces;

    /**
     * Order of coordinates for the ship
     */
    protected HashMap<Integer, Coordinate> coordSeq;
    
    /**
     * Information to show on player's own view 
     */
    protected ShipDisplayInfo<T> myDisplayInfo;

    /**
     * Information to show on enemy's view
     */
    protected ShipDisplayInfo<T> enemyDisplayInfo;

    

    /**
     * Constructor of Basicship
     * 
     * @param where: Coordinates of the ship
     * @param myDisplayInfo: Information to display on my board
     * @param enemyDisplayInfo: Information to display on enemy's board
     */
    public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        int ptr = 0;
        myPieces = new HashMap<Coordinate, Boolean>();
        coordSeq = new HashMap<Integer, Coordinate>();
        for(Coordinate pos: where) {
            myPieces.put(pos, false);
            coordSeq.put(ptr, pos);
            ptr++;
        }
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
    }


    /**
     * Check whether ship occupies Coordinate c
     * 
     * @param c: Coordinate to be checked
     */
    protected void checkCoordinateInThisShip(Coordinate c) {
        if (!occupiesCoordinates(c)) {
            throw new IllegalArgumentException("Ship doesn't contain coordinate (" + c.getRow() + ", " + c.getColumn() + ")");
        }
    }


    /**
     * Check if this ship occupies the given coordinate.
     * 
     * @param where is the Coordinate to check if this Ship occupies
     * @return true if where is inside this ship, false if not.
     */
    public boolean occupiesCoordinates(Coordinate where) {
        return myPieces.containsKey(where);
    }
  
    /**
     * Check if this ship has been hit in all of its locations meaning it has been
     * sunk.
     * 
     * @return true if this ship has been sunk, false otherwise.
     */
    public boolean isSunk() {
        for(Boolean v : myPieces.values()) {
            if(!v) {
                return false;
            }
        }
        return true;
    }
  
    /**
     * Make this ship record that it has been hit at the given coordinate. The
     * specified coordinate must be part of the ship.
     * 
     * @param where specifies the coordinates that were hit.
     * @throws IllegalArgumentException if where is not part of the Ship
     */
    public void recordHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        myPieces.replace(where, true);
        return;
    }
  
    /**
     * Check if this ship was hit at the specified coordinates. The coordinates must
     * be part of this Ship.
     * 
     * @param where is the coordinates to check.
     * @return true if this ship as hit at the indicated coordinates, and false
     *         otherwise.
     * @throws IllegalArgumentException if the coordinates are not part of this
     *                                  ship.
     */
    public boolean wasHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        return myPieces.get(where);
    }
  
    /**
     * Return the view-specific information at the given coordinate. This coordinate
     * must be part of the ship.
     * 
     * @param where is the coordinate to return information for
     * @throws IllegalArgumentException if where is not part of the Ship
     * @return The view-specific information at that coordinate.
     */
    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip) {
        if(myShip)
            return myDisplayInfo.getInfo(where, wasHitAt(where));
        else
            return enemyDisplayInfo.getInfo(where, wasHitAt(where));
    }

   
    /**
     * Get the coordinates occupied by ship
     * 
     * @return: Iterable object of Coordinates occupied by ship
     */
    public Iterable<Coordinate> getCoordinates() {
        return this.myPieces.keySet();
    }

    /**
     * Get relative hit position in ship
     * @return the list of relative position
     */
    public ArrayList<Integer> getHitListSeq() {
        ArrayList<Integer> seq = new ArrayList<Integer>();
        for(int i: coordSeq.keySet()) {
            if(myPieces.get(coordSeq.get(i))) {
                seq.add(i);
            }
        }
        return seq;
    }

    /**
     * Mark the hit point as input sequence for new added ship (mainly used for move)
     * @param seq is the relative position list of hit position
     */
    public void markHitBySeq(ArrayList<Integer> seq) {
        for(int i : seq) {
            myPieces.replace(coordSeq.get(i), true);
        }
        return;
    }
}
