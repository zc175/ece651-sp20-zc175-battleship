package edu.duke.zc175.battleship;


import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the 
 * enemy's board.
 */
public class BoardTextView {

    /**
     * The Board to display
     */
    private final Board<Character> toDisplay;

    /**
     * Constructs a BoardView, given the board it will display.
     * @param toDisplay is the Board to display
     */
    public BoardTextView(Board<Character> toDisplay) {
      this.toDisplay = toDisplay;
      if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
        throw new IllegalArgumentException(
            "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
      }
    }

    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     * @return the String that is the header line for the given board
     */
    String makeHeader() {
      StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
      String sep=""; //start with nothing to separate, then switch to | to separate
      for (int i = 0; i < toDisplay.getWidth(); i++) {
        ans.append(sep);
        ans.append(i);
        sep = "|";
      }
      ans.append("\n");
      return ans.toString();
    }

    /**
     * Make one line of the board view
     * @param height is the line number to make
     * @param line is the identifier of line
     * @param getSquareFn is the function to get information to be showed for specific coordinates
     * @return information of specific line
     */
    public String makeLine(int height, char line, Function<Coordinate, Character> getSquareFn) {
      StringBuilder newLine = new StringBuilder();
      newLine.append("" + line + ' ');
      String sep = "";
      for (int i = 0; i < toDisplay.getWidth(); i++) {
        newLine.append(sep);
        Character res = getSquareFn.apply(new Coordinate(height, i));
        if(res != null) {
          newLine.append(res);
        }
        else {
          newLine.append(' ');
        }
        sep = "|";
      }
      newLine.append(" "+line+'\n');
      return newLine.toString();
    }

    /**
     * Display the board information
     * @param getSquareFn is the function used for getting information of input coordinate to be showed on board
     * @return the string form of board to be displayed
     */
    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
      StringBuilder ans = new StringBuilder();
      String header = makeHeader();
      ans.append(header);
      char line = 'A';
      for(int i = 0; i < toDisplay.getHeight(); i++) {
        ans.append(makeLine(i, line, getSquareFn));
        line = (char) ( (int) line + 1 );
      }
      ans.append(header);
      return ans.toString(); //this is a placeholder for the 
    }

    /**
     * Display the board with player's view
     */
    public String displayMyOwnBoard() {
      return displayAnyBoard((c)->toDisplay.whatIsAtForSelf(c));
    }

    /**
     * Display the board with enemy's view
     */
    public String displayEnemyBoard() {
      return displayAnyBoard((c)->toDisplay.whatIsAtForEnemy(c));
    }

    /**
     * function to get number of splitters for splitting the boards of same line
     * @param number is number of splitter needed
     * @return string form of splitter
     */
    private String generateSplitOfBoard(int number) {
      String s = "";
      for(int i=0; i<number; i++) {
        s += " ";
      }
      return s;
    }

    /**
     * Display the player's board with player's view of enemy's board
     * @param enemyView is enemy's board text veiw
     * @param myHeader is the header of player's board
     * @param enemyHeader is the header of enemy's board
     * @return string form of the display
     */
    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
      String [] myLines = this.displayMyOwnBoard().split("\n");
      String [] enemyLines = enemyView.displayEnemyBoard().split("\n");
      StringBuilder res = new StringBuilder();
      res.append("     " + myHeader);
      res.append(generateSplitOfBoard(2*toDisplay.getWidth()+17-myHeader.length()));
      res.append(enemyHeader + "\n");

      for(int i=0; i<myLines.length; i++) {
        res.append(myLines[i]);
        res.append(generateSplitOfBoard(2*toDisplay.getWidth()+19-myLines[i].length()));
        res.append(enemyLines[i] + "\n");
      }

      return res.toString();
    }
    
  }
