package edu.duke.zc175.battleship;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {

    /**
     * Constructor extend from PlacementRuleChecker
     */
    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }
    
    /**
     * check whether the ship placement is out of bound
     * @param theShip is the ship placement to be checked
     * @param theBoard is the board used
     */
    @Override
    protected void checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        int height = theBoard.getHeight();
        int width = theBoard.getWidth();
        for(Coordinate c : theShip.getCoordinates()) {
            int row = c.getRow();
            int col = c.getColumn();
            if((row < 0 || row >= height) || (col < 0 || col >= width)) {
                // System.out.println("error in bounds: " + row + ", " + col + "\n");
                if(row < 0) {
                    throw new IllegalArgumentException("the ship goes off the top of the board.");
                }
                else if(row >= height) {
                    throw new IllegalArgumentException("the ship goes off the bottom of the board.");
                }
                else if(col < 0) {
                    throw new IllegalArgumentException("the ship goes off the left of the board.");
                }
                else {
                    throw new IllegalArgumentException("the ship goes off the right of the board.");
                }
            }
        }
        return;
    }
}
