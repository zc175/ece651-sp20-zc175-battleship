package edu.duke.zc175.battleship;

import java.util.*;
import java.util.function.Function;
import java.io.*;

public class TextPlayer {

    /**
     * Player's board
     */
    private final Board<Character> theBoard;

    /**
     * View of player's board
     */
    private final BoardTextView view;

    /**
     * Buffer to get input
     */
    private final BufferedReader inputReader;

    /**
     * File stream to output
     */
    private final PrintStream out;

    /**
     * Name of player
     */
    private final String name; 

    /**
     * Factory to create ship
     */
    private final AbstractShipFactory<Character> shipFactory;

    /**
     * List of ships to be placed
     */
    private final ArrayList<String> shipsToPlace;

    /**
     * Hashmap of functions to create corresponding ships
     */
    private final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;


    /**
     * Hashmap that records actions and remain times
     */
    private final HashMap<String, Integer> specialActions = new HashMap<String, Integer>() {{
        put("M", 2);
        put("S", 1);
    }};

    /**
     * Boolean value that records whether this player is computer or not
     */
    private final boolean isComputer;


    /**
     * Constructor to make a player
     * @param name is player's name
     * @param theBoard is player's board
     * @param inputSource is the input source
     * @param out is output file stream
     * @param factory is the factory to create different ships
     * @throws IOException
     */
    public TextPlayer(String name, Board<Character> theBoard, Reader inputSource, PrintStream out, AbstractShipFactory<Character> factory) throws IOException {
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = new BufferedReader(inputSource);
        this.out = out;
        this.shipFactory = factory;
        this.name = name;
        this.shipsToPlace = new ArrayList<String>();
        this.shipCreationFns = new HashMap<String, Function<Placement, Ship<Character>>>();
        this.isComputer = readIsComputer();
    }

    /**
     * Read input and judge whether this player is computer or not
     * @return boolean value that whether this player is computer or not
     * @throws IOException
     */
    private boolean readIsComputer() throws IOException {
        while(true) {
            this.out.print("Is player " + name + " a computer?\n");
            String s = inputReader.readLine();
            s = s.toUpperCase();
            if (s.equals("Y")) {
                return true;
            }
            else if(s.equals("N")) {
                return false;
            }
            else {
                out.print("Input is illegal!\n");
            }
        }
    }

    /**
     * Set up the map between different kinds of ships and their creating functions
     */
    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
        shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
    }

    /**
     * Set up the list of ships to place
     */
    protected void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }

    /**
     * Print function that only print things for human players
     * @param s is the information to print
     */
    protected void Print(String s) {
        if(!isComputer) {
            out.print(s);
        }
    }

    /**
     * Generate random coordinates for computer player
     * @return the generated random coordinate
     */
    public Coordinate generateRandomCoordinate() {
        int row = (int) (Math.random()*26);
        int col = (int) (Math.random()*10);
        return new Coordinate(row, col);
    }
    /**
     * Generate random placement for computer player
     * @return generated ramdom placement
     */
    public Placement generateRandomPlacement() {
        Coordinate c = generateRandomCoordinate();
        char[] selection = {'U', 'D', 'R', 'L', 'H', 'V'};
        int idx = (int) (Math.random()*6);
        return new Placement(c, selection[idx]);
    }


    /**
     * Print information and get placement
     * @param prompt is information to be printed
     * @return the placement of the ship
     * @throws IOException
     */
    public Placement readPlacement(String prompt) throws IOException {
        Print(prompt + '\n');
        if (isComputer) {
            return generateRandomPlacement();
        }
        String s = inputReader.readLine();
        // System.out.println(s);
        return new Placement(s);
    }

    /**
     * Print information and get coordinate
     * @param prompt is information to be printed
     * @return the input coordinate
     * @throws IOException
     */
    public Coordinate readCoordinate(String prompt) throws IOException {
        Print(prompt + '\n');
        if (isComputer) {
            return generateRandomCoordinate();
        }
        String s = inputReader.readLine();
        // System.out.println(s);
        return new Coordinate(s);
    }

    /**
     * Place one ship to the board
     * @param shipName is type of ship to be placed
     * @param createFn is creating funtion of the ship
     * @throws IOException
     */
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn, ArrayList<Integer> hitSeq) throws IOException {
        while(true) {
            try {
                Placement p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
                Ship<Character> s = createFn.apply(p);
                theBoard.tryAddShip(s);
                if(hitSeq != null) {
                    s.markHitBySeq(hitSeq);
                }
                return;
            }
            catch(IllegalArgumentException e) {
                Print("That placement is invalid: " + e.getMessage() + "\n");
            }
        }
        
    }

    /**
     * Generate splitter of the output
     */
    public void printSplit(boolean shouldPrint) {
        if(shouldPrint) {
            out.print("---------------------------------------------------------------------------" + "\n");
        }
    }

    /**
     * Generate general guildence
     */
    public void printGenericInfo() {
        Print("Player " + name + ": you are going to place the following ships (which are all\n" +
                  "rectangular). For each ship, type the coordinate of the upper left\n" +
                  "side of the ship, followed by either H (for horizontal) or V (for\n" +
                  "vertical).  For example M4H would place a ship horizontally starting\n"+
                  "at M4 and going to the right.  You have\n");
        Print("\n");
        Print("2 \"Submarines\" ships that are 1x2\n");
        Print("3 \"Destroyers\" that are 1x3\n");
        Print("3 \"Battleships\" that are 1x4\n");
        Print("2 \"Carriers\" that are 1x6\n");
    }

    /**
     * Print general guildence and place all the ships
     * @throws IOException
     */
    public void doPlacementPhase() throws IOException {
        setupShipCreationList();
        setupShipCreationMap();
        printSplit(!isComputer);
        Print(view.displayMyOwnBoard());
        printSplit(!isComputer);
        printGenericInfo();
    
        for (String shipName : shipsToPlace) {
            printSplit(!isComputer);
            doOnePlacement(shipName, shipCreationFns.get(shipName), null);
            Print(view.displayMyOwnBoard());
        }
        return;
    }

    /**
     * return player's board
     */
    public Board<Character> getBoard() {
        return this.theBoard;
    }

    /**
     * return player's name
     */
    public String getName(){
        return this.name;
    }

    /**
     * return player's board view
     */
    public BoardTextView getView() {
        return this.view;
    }


    /**
     * Get coordinate for fire at attack phase
     * @param enemy is enemy player, which can get board, view and name of enemy player
     * @return valid coordinate to fire at
     * @throws IOException
     */
    public Coordinate getCoordinateInput(String prompt, Board<Character> b) throws IOException {
        while(true) {
            try {
                Coordinate c = readCoordinate(prompt);
                if(c.getRow() < 0 || c.getRow() >= b.getHeight() || c.getColumn() < 0 || c.getColumn() >= b.getWidth()) {
                    throw new IllegalArgumentException("input coordinate out of bound");
                }
                return c;
            }
            catch(IllegalArgumentException e) {
                Print("Invalid Coordinate: " + e.getMessage() + "\n");
            }
        }
    }

    /**
     * Move ship to other coordinate
     * @throws IOException
     */
    public void MoveShip() throws IOException {
        // out.println("Entering move!");
        while (true) {
            try {
                Coordinate c = getCoordinateInput("Player "+name+", which ship do you want to move?", theBoard);
                Ship<Character> s = theBoard.getShipAt(c);
                if(s != null){
                    ArrayList<Integer> hitSeq = s.getHitListSeq();
                    String shipName = s.getName();
                    theBoard.removeShip(s); 
                    doOnePlacement(shipName, this.shipCreationFns.get(shipName), hitSeq);
                    return;
                }
                else {
                    throw new IllegalArgumentException("Input coordinate contains no ship!\n");
                }
            }
            catch (IllegalArgumentException e) {
                Print(e.getMessage());
            }
            
        }

    }

    /**
     * Scan the board
     * @param enemyBoard is enemy's board to be scanned
     * @throws IOException
     */
    public void Scan(Board<Character> enemyBoard) throws IOException {
        while(true) {
            try {
                Coordinate c = getCoordinateInput("Player "+name+", where do you want to scan?", enemyBoard);
                
                // out.print(c.toString());
                HashMap<String, Integer> res = enemyBoard.ScanAt(c);
                for(String s : res.keySet()) {
                    Print(s + "s occupy " + res.get(s) + " squares\n");
                }
                return;

            }
            catch (IllegalArgumentException e) {
                Print(e.getMessage());
            }
        }
        
    }

    /**
     * Fire function that fire at coordinate on enemy's board
     * @param enemyBoard is enemy's board to be scanned
     * @throws IOException
     */
    public void Fire(Board<Character> enemyBoard) throws IOException {
        Coordinate c = getCoordinateInput("Player " + name + " where do you want to fire?", enemyBoard);
        Ship<Character> s = enemyBoard.fireAt(c);
        if(s != null) {
            if(!isComputer) {
                out.print("You hit a " + s.getName() + "!\n");
            }
            else {
                out.print("Player " + name + " hit your " + s.getName() + " at " + c.toString() + "!\n");
            }
        }
        else {
            if(!isComputer) {
                out.print("You missed!\n");
            }
            else {
                out.print("Player " + name + " missed!\n");
            }
            
        }
    }

    /**
     * Possible actions that player can do
     * @param enemy is enemy player
     * @throws IOException
     */
    public void PossibleActions(TextPlayer enemy) throws IOException {
        if(specialActions.size() == 0) {
            Fire(enemy.getBoard());
            return;
        }
        String candidates = "F";
        Print("Possible actions for Player " + name + ":\n\n");
        Print(" F Fire at a square\n");
        if(specialActions.containsKey("M") && specialActions.get("M") > 0) {
            Print(" M Move a ship to another square (" + specialActions.get("M").toString() + " remaining)\n");
            candidates += "M";
        }
        if(specialActions.containsKey("S") && specialActions.get("S") > 0) {
            Print(" S Sonar scan (" + specialActions.get("S") + " remaining)\n");
            candidates += "S";
        }
        // out.print(candidates + "\n");
        String action = "";

        while(true) {
            Print("\nPlayer " + name + ", what would you like to do?\n");
            String s = "";
            if (isComputer) {
                int idx = (int) (Math.random()*candidates.length());
                s += candidates.charAt(idx);
            }
            else {
                s = inputReader.readLine();
                s = s.toUpperCase();
            }
            
            // out.println(s);
            if (s.length() == 1 && candidates.contains(s)) {
                action = s;
                break;
            } 
            else {
                Print("Wrong input for action!\n");
            }
        }

        if(action.equals("F")) {
            Fire(enemy.getBoard());
            return;
        }
        else {
            if(isComputer) {
                out.print("Player "+name+" used a special action\n");
            }
            int val = specialActions.get(action);
            if(val-1 == 0) {
                specialActions.remove(action);
            }
            else {
                specialActions.replace(action, val-1);
            }
            
            if (action.equals("M")) {
                // out.println("Going to enter move function");
                MoveShip(); 
                Print(view.displayMyBoardWithEnemyNextToIt(enemy.getView(), "Your ocean", "Player " + enemy.getName()));
            }
            else if(action.equals("S")) {
                Scan(enemy.getBoard());
                // System.out.println("Return from scan!");
            }
        }
        return;
    }

    


    /**
     * Play one ture, show the current boards, get coordinate to fire at, fire and show attack result
     * @param enemy is enemy player
     * @throws IOException
     */
    public void playOneTurn(TextPlayer enemy) throws IOException {
        printSplit(true);
        out.print("Player " + name + "'s turn:\n");
        
        Print(view.displayMyBoardWithEnemyNextToIt(enemy.getView(), "Your ocean", "Player " + enemy.getName()));
        
        PossibleActions(enemy);
        return;
    }
}
