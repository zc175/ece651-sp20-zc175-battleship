package edu.duke.zc175.battleship;

public class Placement {

    /**
     * Coordinate of the ship
     */
    private final Coordinate where;

    /**
     * orientation of the ship
     */
    private final char orientation;

    /**
     * Possible values for orientation
     */
    private final String checkValue = "VHURDL";


    /**
     * Constructor of ship placement
     * @param init_where is the coordinate of the placement
     * @param init_orientation is the direction (V for Vertical, H for Horizontal) of the ship
     */
    public Placement(Coordinate init_where, char init_orientation) {
        this.where = init_where;
        this.orientation = Character.toUpperCase(init_orientation);
        if(!this.checkValue.contains(String.valueOf(this.orientation))){ 
        // if(this.orientation != 'V' && this.orientation != 'H') {
            // throw new IllegalArgumentException("Orientation must be either 'V' or 'H', but is " + this.orientation);
            throw new IllegalArgumentException("it does not have the correct format.");
        }
    }

    /**
     * Construtor of string form
     * @param descr is the string form input of the placment
     */
    public Placement(String descr) {
        if(descr.length() != 3) {
            // throw new IllegalArgumentException("Wrong string length for Placement initialization: " + descr);
            throw new IllegalArgumentException("it does not have the correct format.");
        }
        String sForWhere = descr.substring(0, 2);
        char init_orientation = descr.charAt(2);
        this.where = new Coordinate(sForWhere);
        this.orientation = Character.toUpperCase(init_orientation);
        if(!this.checkValue.contains(String.valueOf(this.orientation))){ 
        // if(this.orientation != 'V' && this.orientation != 'H') {
            // throw new IllegalArgumentException("Orientation must be either 'V' or 'H', but is " + this.orientation);
            throw new IllegalArgumentException("it does not have the correct format.");
        }
    }

    /**
     * Return coornidate of placement
     */
    public Coordinate getWhere() {
        return this.where;
    }

    /**
     * return orientation of placement
     */
    public char getOrientation() {
        return this.orientation;
    }

    /**
     * return string form of placement
     */
    public String toString() {
        StringBuilder ans = new StringBuilder();
        ans.append(where.toString());
        return ans.toString() + orientation;
    }

    /**
     * judge whether object o is same as this placement (same coordinate, same orientation)
     */
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Placement p = (Placement) o;
            return where.equals(p.where) && orientation == p.orientation;
        }
        return false;
    }

    /**
     * hashcode form of placement
     */
    public int hashCode() {
        return toString().hashCode();
    }


    
}
