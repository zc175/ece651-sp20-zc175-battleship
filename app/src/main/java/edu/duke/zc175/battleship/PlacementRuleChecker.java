package edu.duke.zc175.battleship;

public abstract class PlacementRuleChecker<T> {

    /**
     * next rule to check
     */
    private final PlacementRuleChecker<T> next;

    /**
     * Default constructor
     */
    public PlacementRuleChecker(PlacementRuleChecker<T> next) {
        this.next = next;
    }

    /**
     * abstract method to check rules, need to be override
     */
    protected abstract void checkMyRule(Ship<T> theShip, Board<T> theBoard);

    /**
     * Check whether the placement is valid using rules defined
     * @param theShip is the ship placement to be checked
     * @param theBoard is the board used
     */
    public void checkPlacement (Ship<T> theShip, Board<T> theBoard) {
        checkMyRule(theShip, theBoard);
        if(next != null) {
            next.checkMyRule(theShip, theBoard);
        }
        return;
    }
}
