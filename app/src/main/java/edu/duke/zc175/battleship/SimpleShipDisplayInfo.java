package edu.duke.zc175.battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {

    /**
     * original data showed on board
     */
    private T myData;

    /**
     * data showed on hit
     */
    private T onHit;

    /**
     * Constructor
     */
    public SimpleShipDisplayInfo(T Data, T OnHit) {
        myData = Data;
        onHit = OnHit;
    }

    /**
     * Return coordinate information on board, show whether hit or not
     * @param where is the coordinate to get information
     * @param hit is whether the position is hit or not
     */
    public T getInfo(Coordinate where, boolean hit) {
        if(hit) {
            return onHit;
        }
        else {
            return myData;
        }
    }
}
